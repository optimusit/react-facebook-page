// Core
import React, { Component } from 'react';
import { Consumer } from '../../components/HOC/withProfile';
import Styles from './styles.m.css';

export default class Composer extends Component {
    render () {
        return (
            <Consumer>
                {(context) => (
                    <section className = { Styles.composer }>
                        <img src = { context.avatar } />
                        <form>
                            <textarea placeholder = 'mind' />
                            <input type = 'submit' value = 'post' />
                        </form>
                    </section>
                )}
            </Consumer>
        );
    }
}
